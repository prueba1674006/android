package com.example.botones

import android.graphics.drawable.PaintDrawable
import android.graphics.drawable.PictureDrawable
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.botones.ui.theme.BotonesTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            BotonesTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Greeting("Android")
                }
            }
        }
    }
}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "Hello $name!",
        modifier = modifier
    )
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    BotonesTheme {
        val vacio = painterResource(id = R.drawable.vacio)
        val plantas = painterResource(id = R.drawable.plantas)
        val frutas = painterResource(id = R.drawable.frutas)

        var num by remember { mutableIntStateOf(0) }
        var contador by remember { mutableIntStateOf(0) }

        if (contador==0){
            Image(modifier = Modifier.fillMaxWidth(0.8f), painter = vacio, contentDescription = "actual")
        }else if(contador==1){
            Image(modifier = Modifier.fillMaxWidth(0.8f),painter = plantas, contentDescription = "actual")
        }else if (contador==2){
            Image(modifier = Modifier.fillMaxWidth(0.8f),painter = frutas, contentDescription = "actual")
        }

        Column (modifier = Modifier.padding(0.dp,80.dp,0.dp,0.dp)){
            Button(onClick ={contador++}) { Text("Boton")
                if(contador>=3){
                    num=(1..5).random()
                    contador=0
                }
            }
            Text(text ="Ganancias: "+ num.toString())
        }
    }
}