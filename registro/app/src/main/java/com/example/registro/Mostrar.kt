package com.example.registro

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.registro.ui.theme.RegistroTheme

class Mostrar : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            RegistroTheme() {
                // A surface container using the 'background' color from the theme
                Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background) {
                    EjClase()
                }
            }
        }
    }
}

@Composable
fun Huerto(){
    var num by remember {
        mutableStateOf(0)
    }
    var cont by remember {
        mutableStateOf(1)
    }
    var id by remember {
        mutableStateOf(R.drawable.vacio)
    }



    Column(verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally) {
        Image(painter = painterResource(id = id),
            contentDescription = "Huerto",
            modifier = Modifier.size(300.dp),
            contentScale = ContentScale.FillWidth)

        Button(

            onClick = {
                if (cont == 1){
                    id = R.drawable.plantas
                    cont = 2
                }else if(cont == 2){
                    id = R.drawable.frutas
                    cont = 3
                    num += (1..5).random()
                }else if (cont == 3){
                    id = R.drawable.vacio
                    cont = 1
                }

            })
        {
            Text(text = "PULSA")
        }
        Text("Ganancias: " + num.toString() + "$",)

    }

}
@Preview(showBackground = true)
@Composable
fun EjClase() {
    RegistroTheme() {
        Huerto()
    }
}