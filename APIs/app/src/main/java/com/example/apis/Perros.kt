package com.example.apis

class Perros {
    private lateinit var mensaje: String
    private lateinit var status: String
    fun getMensaje() : String{
        return this.mensaje
    }
    fun getStatus() : String {
        return this.status
    }
    fun setMensaje(mensaje : String){
        this.mensaje = mensaje
    }
    fun setStatus(status : String){
        this.status = status
    }
}