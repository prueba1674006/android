package com.example.apis

import android.telecom.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path

interface PerroAPI {
    @Headers ("Accept: application/json")
    @GET ("breeds/image/random")
    fun getPerro(): retrofit2.Call<Perros>
}